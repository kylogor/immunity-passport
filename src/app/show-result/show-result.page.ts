import { Component, OnInit } from '@angular/core';
import {ClientInfoService} from '../../services/client-info.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-show-result',
  templateUrl: './show-result.page.html',
  styleUrls: ['./show-result.page.scss'],
})
export class ShowResultPage implements OnInit {

  fname;
  lname;
  birth;
  address;
  phoneNumber;
  email;

  vaccine;
  date;
  clinic;
  dose;
  client_name;
  nameCredential = 'Immunity Passport';

  constructor(
    private router: Router,
    public clientInfo: ClientInfoService,
  ) {
    this.client_name = this.clientInfo.clientInfo[0].content[0][1] + " " + this.clientInfo.clientInfo[0].content[1][1];
    let content = this.clientInfo.clientInfo[0].content;
      this.fname = content[0][1];
      this.lname = content[1][1];
      this.birth = content[2][1];
      this.address = content[3][1];
      this.phoneNumber = content[4][1];
      this.email = content[5][1];

      this.vaccine =  content[8][1];
      this.date = content[9][1];
      this.clinic = content[10][1];
      this.dose =content[11][1];
   }



  ngOnInit() {
  }

  checkResult(param: string){
    if (param == "hard"){
      this.clientInfo.lightVerification = false;
    }
    else if (param == "light"){
      this.clientInfo.lightVerification = true;
    }
    this.router.navigate(['/check-result']);
  }

  applyForm(){
    this.router.navigate(['/home']);
  }

}
