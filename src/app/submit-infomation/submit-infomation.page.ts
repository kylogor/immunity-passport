import { Component} from '@angular/core';
import {CredentialRequestMessage, Credentials} from 'siriusid-sdk';
import {ClientInfoService} from '../../services/client-info.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import {ChangeDataService} from '../../services/change-data.service';

@Component({
  selector: 'app-submit-infomation',
  templateUrl: './submit-infomation.page.html',
  styleUrls: ['./submit-infomation.page.scss'],
})
export class SubmitInfomationPage{

  img = null;
  URL;
  status = null;
  lived = false;
  subscribe;
  flag = true;
  continue = true;
  connect = false;

  linkLogin;
  linkCredential;

  fname;
  lname;
  birth;
  address;
  phoneNumber;
  email;
  motherName;
  fatherName;

  /** Immunity Passport Information*/
  vaccine;
  date;
  clinic;
  dose;
  aggree;
  credential_id = "immunity_passport";
  nameCredential = 'Immunity Passport'


  constructor(
    private router: Router,
    public clientInfo: ClientInfoService,
    public alertController: AlertController,
    private changeDataService:ChangeDataService
    ) {
      let content = this.clientInfo.clientInfo[0].content;
      this.fname = content[0][1];
      this.lname = content[1][1];
      this.birth = content[2][1];
      this.address = content[3][1];
      this.phoneNumber = content[4][1];
      this.email = content[5][1];
      this.motherName = content[6][1];
      this.fatherName = content[7][1];
  }

  // for QR code
  async createCredential(){
    if (this.vaccine && this.date && this.clinic && this.dose && this.aggree){
      this.continue = true;
      let content = new Map<string,string>([
        ['First Name', this.fname],
        ['Last Name', this.lname],
        ['Date of Birth', this.birth],
        ['Address', this.address],
        ['Phone number', this.phoneNumber],
        ['Email', this.email],
        ["Mother's Name", this.motherName],
        ["Father's Name", this.fatherName],
        ['Vaccine', this.vaccine],
        ['Date given', this.date],
        ['Doctor Office or Clinic', this.clinic],
        ['Next dose due', this.dose],
      ])
      const credential = Credentials.create(
        this.credential_id,
        this.nameCredential,
        'ProximaXCity Health Department',
        'http://icon-library.com/images/immunity-icon/immunity-icon-28.jpg',
        [],
        content,
        "Passport and Visa",
        Credentials.authCreate(content,this.changeDataService.privateKeydApp)

      );
      const msg = CredentialRequestMessage.create(credential);
      console.log('-----------content',content);
  
      this.flag = false;
      this.img = await msg.generateQR();
      this.linkCredential = await msg.universalLink();
    }
    else {
      this.alertWarning();
    }

    
  }


  async alertWarning() {
    const alert = await this.alertController.create({
      header: 'Warning!',
      message: 'Fill these informations to continue',
      buttons: [
        {
          text: 'Ok' ,
          cssClass: 'secondary',
          handler: () => {
          }
        }
      ],
      cssClass:"boxAlert"
    });

    await alert.present();
  }

  welcome(){
    this.clientInfo.haveCredential = true;
    this.router.navigate(['/welcome']);
  }

  ionViewWillEnter(){
    //console.log("enter");
    this.flag = true;
  }


  checkResult(param:string){
    if (param == "hard"){
      this.clientInfo.lightVerification = false;
    }
    else if (param == "light"){
      this.clientInfo.lightVerification = true;
    }
    this.router.navigate(['/check-result']);
  }

  applyForm(){
    this.router.navigate(['/home']);
  }

  deeplink(param:string){
    if (param == "login"){
      window.location = this.linkLogin
    }
    else {
      window.location = this.linkCredential
    }
  }
}
