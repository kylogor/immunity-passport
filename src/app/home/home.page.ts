import { Component} from '@angular/core';
import {LoginRequestMessage, VerifytoLogin,ApiNode} from 'siriusid-sdk';
import {Listener, TransferTransaction, EncryptedMessage, PublicAccount,NetworkType} from 'tsjs-xpx-chain-sdk';
import {ChangeDataService} from '../../services/change-data.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import {ClientInfoService} from 'src/services/client-info.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  linkLogin;
  linkCredential;

  img = null;
  showImg = false;
  URL;
  //"https://demo-sc-api-1.ssi.xpxsirius.io/"
  currentNode = "demo-sc-api-1.ssi.xpxsirius.io";//"bctestnet1.brimstone.xpxsirius.io";
  status = null;
  lived = false;
  subscribe;
  listener: Listener;
  connect = false;
  credential_id = "passport";
  alert = true;
  warning = false;
  flag = true;
  step1 = false;
  nameCredential = "Immunity Passport";

  constructor(
    private changeDataService: ChangeDataService,
    private router: Router,
    public alertController: AlertController,
    private clientInfo: ClientInfoService
    ) {
      console.log(changeDataService.addressdApp);
    //this.loginRequestAndVerify();
  }

  ionViewWillEnter(){
    ApiNode.apiNode = "https://"+this.currentNode;
    ApiNode.networkType = NetworkType.PRIVATE_TEST;
    this.changeDataService.apiNode = this.currentNode;
    this.changeDataService.updateWebsocket();
    this.listener = new Listener(this.changeDataService.ws, WebSocket);
    if(this.flag){
      this.presentAlert2();
      this.flag = false;
    } else {
      this.step1 = false;
      this.presentAlert();
    }
  
  }
  // for QR code
  async loginRequestAndVerify(){
    this.connect = false;
    this.step1 = true;

    const loginRequestMessage = LoginRequestMessage.create(this.changeDataService.publicKeydApp,[this.credential_id]);
    const sessionToken = loginRequestMessage.getSessionToken();
    //console.log('sessionToken = ' + sessionToken);
    this.img = await loginRequestMessage.generateQR();
    this.linkLogin = await loginRequestMessage.universalLink();
    
    this.connection(sessionToken);
    let interval = setInterval(async () => {
      const listenerAsAny = this.listener as any;
      console.log(listenerAsAny.webSocket.readyState);
      if (listenerAsAny.webSocket.readyState !== 1 && listenerAsAny.webSocket.readyState !== 0 && !this.connect){
        this.warning = true;
        this.listener = new Listener(this.changeDataService.ws, WebSocket);
        this.connection(sessionToken);
        // this.img = false;
      }
      else if(listenerAsAny.webSocket.readyState == 1 && !this.connect){
        this.showImg = true;
      }
      else if(this.connect){
        clearInterval(interval);
      }
    }, 1000);
  }
  
  async connection(sessionToken) {
    this.listener.open().then(() => {
      this.warning = false;
      this.subscribe = this.listener.confirmed(this.changeDataService.addressdApp).subscribe(transaction => {
        VerifytoLogin.verify(transaction, sessionToken,[this.credential_id],this.changeDataService.privateKeydApp).then(
          verify=>{
            if (verify){
              this.connect = true;
              console.log("Transaction matched");
              this.changeDataService.addressSiriusid = transaction.signer.address.plain();
              this.clientInfo.clientInfo = VerifytoLogin.credentials;
              this.subscribe.unsubscribe();
              this.router.navigate(['/submit-infomation']);
            }
            else console.log("Transaction not match");   
          }
        )
              
      });
    })
    
  }

  refresh(){
    this.subscribe.unsubscribe();
    this.loginRequestAndVerify();
  }
  // login(){
  //   this.loginRequestAndVerify();
  // }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'We will ask you to share',
      message: '<div class="alert"><img style="width:10%" src="assets/icon-credentials-digital-passport.svg" ><p>ProximaXCity Passport</p></div>',
      buttons: [
        {
        
          text: 'Login' ,
          cssClass: 'primary',
          handler: () => {
            this.loginRequestAndVerify();
          }
        }
      ],
      cssClass:"boxAlert1"
    });

    await alert.present();
  }

  async presentAlert2() {
    const alert = await this.alertController.create({
      header: 'Welcome to ProximaXCity ' + this.nameCredential,
      buttons: [
        {
          text: 'Create '+ this.nameCredential ,
          cssClass: 'primary',
          handler: () => {
            this.presentAlert();
          }
          
        },
        {
          text: 'Hard Verification' ,
          cssClass: 'primary',
          handler: () => {
            this.checkResult('hard');
            // this.router.navigate(['/check-result']);
          }
        },
        {
          text: 'Light Verification' ,
          cssClass: 'primary',
          handler: () => {
            this.checkResult('light');
            // this.router.navigate(['/check-result']);
          }
        },
      ],
      
      cssClass:"boxAlert"
    });

    await alert.present();
  }

  checkResult(param:string){
    if (param == "hard"){
      this.clientInfo.lightVerification = false;
    }
    else if (param == "light"){
      this.clientInfo.lightVerification = true;
    }
    this.connect = true;
    // this.subscribe.unsubscribe();
    this.router.navigate(['/check-result']);
  }

  deeplink(param:string){
    if (param == "login"){
      window.location = this.linkLogin
    }
    else {
      window.location = this.linkCredential
    }
  }

}
