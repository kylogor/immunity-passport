import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmitInfomationPage } from './submit-infomation.page';

describe('SubmitInfomationPage', () => {
  let component: SubmitInfomationPage;
  let fixture: ComponentFixture<SubmitInfomationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmitInfomationPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitInfomationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
