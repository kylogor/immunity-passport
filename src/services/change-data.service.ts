import { Injectable } from '@angular/core';
import { Address, NetworkType } from 'tsjs-xpx-chain-sdk';

@Injectable({
  providedIn: 'root'
})
export class ChangeDataService {

  pubickeySiriusid =''; 
  addressSiriusid = '';
  name = ''; // Name of user when him sign up
  
  /**
   * For TEST_NET
   */
  publicKeydApp = '949EA464C9D7AB425443973B4555A0E692776CD5A696DA40D548FE0E1B9CEBA9'; 
  privateKeydApp = '2C89CF31CE574A581EF55D89AD70DF20EE25F9B9E3852437A153ED31572FC78E';
  addressdApp: Address;
  apiNode = "";
  ws = 'wss://' + this.apiNode; //websocket
  //ws = "wss://bctestnet3.xpxsirius.io";
  
  /**
   * For local testing
   */
  // ws = 'ws://192.168.1.23:3000'; //local
  // publicKeydApp = '0750E2716F2CBADD19F9F5314944ABFDF005A5D3D6DAAB4D08E97F188AAA9300'; // privateKey: 4EBACFE2B723FDA8B4DFC7E7B8AA26AF9C8638DCC353463AFF89F03230AFD38E
  updateWebsocket(){
    this.ws = 'wss://' + this.apiNode;
  }
  constructor() { 
    this.addressdApp = Address.createFromPublicKey(this.publicKeydApp, NetworkType.PRIVATE_TEST);
  }
}
