import { Component, OnInit } from '@angular/core';
import {ClientInfoService} from '../../services/client-info.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {

  client_name;
  nameCredential = 'Immunity Passport';
  constructor(
    public clientInfo: ClientInfoService,
    private router: Router,
  ) {
    this.client_name = this.clientInfo.clientInfo[0].content[0][1] + " " + this.clientInfo.clientInfo[0].content[1][1];
   }

  ngOnInit() {
  }
  checkResult(param){
    if (param == "hard"){
      this.clientInfo.lightVerification = false;
    }
    else if (param == "light"){
      this.clientInfo.lightVerification = true;
    }
    this.router.navigate(['/check-result']);
  }

  applyForm(){
    this.router.navigate(['/home']);
  }
}
